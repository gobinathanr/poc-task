import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfig } from '../app.config.serice';


@Injectable({
  providedIn: 'root'
})
export class DetailsService {

  constructor(private http: HttpClient, private config: AppConfig) { }

  getdetails() {
    return this.config.get('employees');
  }

  getiddeatils(id: string) {
    return this.config.get('employee/' + id);
  }
}
