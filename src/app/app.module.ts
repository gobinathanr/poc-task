import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { appRoutingModule } from './app.routing';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { fakeBackendProvider } from './helpers';
import { AppComponent } from './app.component';
import { BasicAuthInterceptor, ErrorInterceptor } from './helpers';
import { LoginComponent } from './login/login.component';
import { HomeModule } from './home/home.module';
import { DetailsService } from './services/details.service';
import { AppConfig } from './app.config.serice';


@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        appRoutingModule,
        HomeModule,
    ],
    declarations: [
        AppComponent,
        LoginComponent,
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        DetailsService,
        AppConfig,
        fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(applicationRef: ApplicationRef) {
        const originalTick = applicationRef.tick;
        applicationRef.tick = function () {
            const windowPerfomance = window.performance;
            const before = windowPerfomance.now();
            const retValue = originalTick.apply(this, arguments);
            const after = windowPerfomance.now();
            const runTime = after - before;
            window.console.log('CHANGE DETECTION TIME', runTime);
            return retValue;
        };
    }
}