import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable()
export class AppConfig {
    ROOT_API: string;

    constructor(private http: HttpClient) {
    }

    get(apiName: string, ...parameters: any[]): Observable<any> {
     this.ROOT_API = environment.ROOT_API;
     const url = `${this.ROOT_API}/${apiName}/` + parameters.join('/');
     const filter = url.substring(0, url.length - 1);
     const result = this.http.get(filter);
     return result;
    }

}