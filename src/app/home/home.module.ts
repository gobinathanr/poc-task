import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HomeRoutingModule } from './home-routing.module';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    HomeRoutingModule,
    MaterialModule,
    RouterModule,
    FormsModule
  ],
  declarations: [],
  schemas : [CUSTOM_ELEMENTS_SCHEMA]
})
export class HomeModule { }
