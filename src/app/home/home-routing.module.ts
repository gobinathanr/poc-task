import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { PatientdetailsComponent } from './patientdetails/patientdetails.component';
import { PatientinfoComponent } from './patientinfo/patientinfo.component';

const routes: Routes = [
 { path : '', component : HomeComponent,
   children : [
     {path : '' , component : PatientdetailsComponent }
   ]
},
{path : 'patientinfo/:id' , component : PatientinfoComponent}
];



@NgModule({
  declarations : [ HomeComponent , PatientdetailsComponent, PatientinfoComponent],
  imports: [ BrowserModule , FormsModule, MaterialModule,
  RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
