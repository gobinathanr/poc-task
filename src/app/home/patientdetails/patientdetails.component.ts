import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';


export interface PeriodicElement {
  id: string;
  name: string;
  age: string;
}

@Component({
  selector: 'app-patientdetails',
  templateUrl: './patientdetails.component.html',
  styleUrls: ['./patientdetails.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PatientdetailsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'age'];

  @Input() dataSource: any = [];

  id: string;

  constructor() { }

  ngOnInit() {
    this.dataSource.map((x) => {
      this.id = x.id;
    });
  }


}
