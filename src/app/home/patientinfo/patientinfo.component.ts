import { Component, OnInit } from '@angular/core';
import { DetailsService } from '../../services/details.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
@Component({
    selector: 'app-patientinfo',
    templateUrl: './patientinfo.component.html',
    styleUrls: ['./patientinfo.component.css'],
})

export class PatientinfoComponent implements OnInit {

    getid: string;
    results: any = [];

    constructor(private details: DetailsService, private route: ActivatedRoute, private router: Router,
        private auth: AuthenticationService) { }

    ngOnInit() {
        this.getid = this.route.snapshot.paramMap.get('id');
        let promise = new Promise((resolve, reject) => {
            this.details.getiddeatils(this.getid).toPromise().then(
                res => {
                    this.results.push(res.data);
                    resolve();
                }, msg => {
                    reject(msg);
                });
        });
        return promise;
    }

    patientById ( index, results ) {
      return results ? results.id : undefined;
    }


    signout() {
        this.auth.logout();
        this.router.navigate(['/login']);
    }

}