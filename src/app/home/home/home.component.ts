import { Component, OnInit, ComponentFactoryResolver, ViewChild, ViewContainerRef, OnDestroy } from '@angular/core';
import { PatientdetailsComponent } from '../patientdetails/patientdetails.component';
import { AuthenticationService } from '../../services';
import { DetailsService } from '../../services/details.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SubscriptionLike } from 'rxjs';
import * as signalR from '@aspnet/signalr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  @ViewChild('dynamic', { read: ViewContainerRef }) dynamic: ViewContainerRef;

  subscription: SubscriptionLike;
  newPatiendClicked = true;
  model: any = {};
  model2: any = {};
  Resultdata: any[];
  Arrayrecord: any = [];
  image: any;

  private url: string = 'http://localhost:58867/alerts';

  constructor(private auth: AuthenticationService, private router: Router, private componentFactoryResolver: ComponentFactoryResolver,
    private details: DetailsService, route: ActivatedRoute, ) {
  }

  ngOnInit() {
    const connection = new signalR.HubConnectionBuilder()
      .configureLogging(signalR.LogLevel.Debug)
      .withUrl(this.url)
      .build();
    connection.start().then(function () {
      console.log('Connected!');
    }).catch(function (err) {
      return console.error(err.toString());
    });
    connection.on('BroadcastMessage', (type: string, payload: string) => {

    });

    this.subscription = this.details.getdetails().subscribe(res => {
      this.Resultdata = res.data;
    });

  }

 addNewDetailsBtn() {
    this.newPatiendClicked = !this.newPatiendClicked;
  }

  addPatient() {
    const modifyArr = {
      id : this.model.id,
      employee_name: this.model.name,
      employee_age: this.model.age
    };
    this.Arrayrecord.push(modifyArr);
    const result = [...this.Arrayrecord, ...this.Resultdata];
    this.dynamic.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(PatientdetailsComponent);
    const componentref = this.dynamic.createComponent(componentFactory);
    componentref.instance.dataSource = result;
  }


  signout() {
    this.auth.logout();
    this.router.navigate(['/login']);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }


}

